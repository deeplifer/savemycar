from peewee import *
from ConfigParser import SafeConfigParser

parser = None
db_name = None
db = SqliteDatabase(None)


def initialize(_db_name=None):
	global parser
	global db_name

	parser = SafeConfigParser()
	parser.read('config.cfg')

	if _db_name is None:
		db_name = parser.get('database', 'name')
		if db_name is None:
			db_name = "default.db"
	else:
		db_name = _db_name

	db.init(db_name)
	User.create_table(True)
	Car.create_table(True)


def closeAll():
	db.close()


class BaseModel(Model):
	class Meta:
		database = db


class User(BaseModel):
	name = CharField()
	chatId = IntegerField(default=0)
	userId = IntegerField(default=0)
	phone = CharField()


class Car(BaseModel):
	nomer = CharField()


def create_user(name='_unknown_', userId=0, chatId=0, phone='0'):
	User.get_or_create(name=name, userId=userId, chatId=chatId, phone=phone)

def get_user_by_chatid(chatId):
	result = None
	try:
		 result = User.select().where(User.chatId == chatId).get()
	except:
		result = None





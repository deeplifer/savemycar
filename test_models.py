import os
import unittest
import models


class TestModelsCommon(unittest.TestCase):
	_db_name = "test_db"

	@classmethod
	def setUpClass(self):
		if os.path.isfile(self._db_name):
			os.remove(self._db_name)

		models.initialize(self._db_name)

	def test_config(self):
		test_string = models.parser.get('test', 'test')
		self.assertEqual(test_string, 'ok')

	def test_db_exists(self):
		self.assertTrue(os.path.isfile(self._db_name))

	def test_case_table_exists(self):
		self.assertTrue(models.User.table_exists())

	@classmethod
	def tearDownClass(self):
		models.closeAll()
		if os.path.isfile(self._db_name):
			os.remove(self._db_name)


class TestModelsUser(unittest.TestCase):
	_db_name = "test_db"
	User1 = None
	User2 = None

	@classmethod
	def setUpClass(self):
		if os.path.isfile(self._db_name):
			os.remove(self._db_name)
		models.initialize(self._db_name)

	def test_case_pass(self):
		self.assertEqual(True,True)

	@classmethod
	def tearDown(self):
		models.closeAll()
		if os.path.isfile(self._db_name):
			os.remove(self._db_name)

if __name__ == '__main__':
	unittest.main()